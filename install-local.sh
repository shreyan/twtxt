#!/bin/bash

# get the current version from lib/twtxt/version.rb
VERSION=`cat lib/twtxt/version.rb | grep "VERSION =" | cut -d '"' -f 2`
echo "Building gem version $VERSION"

# build the gem
gem build twtxt.gemspec

# install the gem locally
gem install ./twtxt-$VERSION.gem