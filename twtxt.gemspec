# frozen_string_literal: true

require_relative "lib/twtxt/version"

lib = File.expand_path("lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

specfiles = Dir["lib/twtxt/*"]
specfiles.push("lib/twtxt.rb")

Gem::Specification.new do |spec|
  spec.name = "twtxt"
  spec.version = Twtxt::VERSION
  spec.authors = ["Shreyan Jain"]
  spec.email = ["shreyan.jain.9@outlook.com"]

  spec.summary = ""
  spec.description = ""
  spec.homepage = "https://shreyan.codeberg.io/twtxt"
  spec.license = "Unlicense"
  spec.required_ruby_version = ">= 3.1.2"

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://codeberg.org/shreyan/twtxt"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = specfiles
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  spec.add_dependency "httparty"

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
