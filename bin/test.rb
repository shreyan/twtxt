require "twtxt"

credentials = Twtxt::Yarn::Credentials.new(ENV["YARN_USERNAME"], ENV["YARN_PASSWORD"], ENV["YARN_POD"])
session = Twtxt::Yarn::Session.new(credentials)
puts session.follow_yarn_profile("@stigatle@yarn.stigatle.no")
