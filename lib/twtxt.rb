# frozen_string_literal: true

require "twtxt/version"
require "twtxt/yarn"
require "twtxt/file"
require "twtxt/entries"
require "twtxt/profile"
# require "twtxt/txt"

module Twtxt
  class Error < StandardError; end

  # Your code goes here...
end
