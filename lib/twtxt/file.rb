require "twtxt"

module Twtxt
  class File
    def initialize(file_path)
      @file_path = file_path
      @entries = []

      File.foreach(@file_path) do |line|
        entry = Twtxt.parse(line.strip)
        @entries << entry if entry
      end
    end

    def entries
      @entries
    end

    def add_entry(entry)
      @entries << entry
    end

    def write
      File.open(@file_path, "w") do |file|
        @entries.each do |entry|
          file.puts(Twtxt.format(entry))
        end
      end
    end
  end
end
