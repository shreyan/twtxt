require "date"

module Twtxt
  class Entry
    attr_reader :text, :date

    def initialize(text:, date:)
      @text = text
      @date = date
    end

    def edit(text) #enter new text to replace old text
      @text = text
    end

    def delete
      @text = nil
      @date = nil
    end
  end

  def self.parse(entry_string)
    if entry_string =~ /^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}[\+\-]\d{2}:\d{2})\t(.+)$/
      date_string = $1
      text = $2
      date = DateTime.iso8601(date_string)

      Entry.new(text: text, date: date)
    else
      nil
    end
  end

  def self.format(entry)
    if entry.date.nil? and entry.text.nil?
      return nil
    else
      date_string = entry.date
      "#{date_string}\t#{entry.text}"
    end
  end
end
