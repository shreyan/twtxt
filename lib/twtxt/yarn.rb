require "httparty"
require "json"

module Twtxt
  module Yarn
    def yarn_twtxt_link(yarn_id)
      profile_components = yarn_id.split("@")
      profile_domain = profile_components[2]
      profile_name = profile_components[1]
      "https://#{profile_domain}/user/#{profile_name}/twtxt.txt"
    end

    def api_endpoint(yarnpod)
      "https://#{yarnpod}/api/v1"
    end

    def get_jwt_token(username, password, endpoint)
      response = HTTParty.post("#{endpoint}/auth", body: { username: username, password: password }.to_json, headers: { "Content-Type" => "application/json" })
      JSON.parse(response.body)["token"]
    end

    module_function :yarn_twtxt_link, :api_endpoint, :get_jwt_token

    def authenticated_headers(token)
      { "Token" => "#{token}", "Content-Type" => "application/json" }
    end

    class Credentials
      attr_reader :username, :password, :yarnpod

      def initialize(username, password, yarnpod)
        @username = username
        @password = password
        @yarnpod = yarnpod
      end
    end

    class Session
      include HTTParty
      include Yarn
      attr_reader :credentials, :endpoint, :token, :headers

      def initialize(credentials)
        @credentials = credentials
        @endpoint = api_endpoint(credentials.yarnpod)
        @token = get_jwt_token(credentials.username, credentials.password, self.endpoint)
        @headers = authenticated_headers(self.token)
      end

      def ping
        self.class.get("#{@endpoint}/ping", headers: @headers)
      end

      def whoami
        self.class.get("#{@endpoint}/whoami", headers: @headers)
      end

      def get_profile(username)
        self.class.get("#{@endpoint}/profile/#{username}", headers: @headers)
      end

      def get_profile_twts(username)
        self.class.get("#{@endpoint}/profile/#{username}", headers: @headers)
      end

      def get_twtxt(profile)
        self.class.get("https://#{credentials.yarnpod}/user/#{profile}/twtxt.txt", headers: @headers)
      end

      def post_twt(twt_text)
        self.class.post("#{@endpoint}/post", body: { text: twt_text, post_as: "" }.to_json, headers: @headers)
      end
    end
  end
end
