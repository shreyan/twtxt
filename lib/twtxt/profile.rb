require "open-uri"

module Twtxt
  class Profile
    attr_reader :path, :twts

    def initialize(path)
      if path.start_with?("http://", "https://")
        @path = path
      else
        Twtxt::Error.new("Invalid path: #{path}")
      end
    end

    def entries
      if @path.start_with?("http://", "https://")
        content = URI.open(@path).read
      end

      content.lines.map do |line|
        Twtxt.parse(line)
      end.compact
    end
  end
end
